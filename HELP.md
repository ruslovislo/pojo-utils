# Getting Started

### небольшая инструкция 

    rules - это тело фильтра. Представляет собой JSON объект.

    rules состоит из следующих параметров:
     1 - operation (обязательно). Это строковое значение AND или OR. Определяет каким оператором сопоставляются отдельные условия фильтрации.
     2 - conditions (обязательно). Это массив JSON объектов. Каждый объект это одно условие фильтрации.
     3 - rules (опционально). Это массив JSON объектов, которые имеют такую же структуру что и рассматриваемый объект rules. Смысл этого параметра - добавить вложенные подзапросы для фильтрации.
     
     пример:
     
     rules:{
        operation:"AND",
        conditions:[
            {...},{...}
        ],
        rules:[
            {...},{...}
        ]
     }
     
     
     Объект conditions (условие фильтрации) состоит из следующих параметров:
     1 - comparison (обязательно). Способ сравнения поля с указанным значением.
     2 - field (обязательно). Сравниваемое поле.
     3 - value (обязательно). Сравниваемое значение.
     
     пример: 
     
     rules:{
             operation:"OR",
             conditions:[
                 {
                    comparison: "eq",
                    field: "firstName",
                    value: "Руслан"
                 },{
                   comparison: "like",
                   field: "lastName",
                   value: "%Руслан%"
                }
             ]
          }
          
     В приведенном выше примере указано условие, при котором значение поля firstName должно быть равно "Руслан" ИЛИ значение поля lastName должно содержать строку "Руслан".
     
     Возможные значения параметра comparison:
     - eq       // equal
     - gt       // greaterThan
     - lt       // lowerThan    
     - ne       // not equal
     - isnull   // is null    
     - notnull  // is not null  
     - in       // in collection      
     - notin    // not in collection      
     - like     // like search string     
     
     Значения параметра field следует брать из тела JSON ответа вызываемого HTTP метода. 
     Например если метод запрашивает список аккаунтов и в ответе представлен список аккаунтов в следующем формате
     
     ...
     {
        firstName: "aaaa",
        lastName: "bbbb",
        weight: 55,
        birthDate: "1990-02-05"
     }
     ...

    то в таком случае в параметр field можно подавать любое значение из списка (firstName,lastName,weight,birthDate).
    
    Значение параметра value может принимать любые значения, но тип значения завист от фильруемого поля. Т.е. если в параметр firstName представлены строковые значения, то и в параметр value так же надо указывать строковые значения.
    {
        comparison: "eq",
        field:"firstName",
        value:"aaaa"
    }
    
    В случае с параметром field = weight, в котором представлено числовое значение, в параметр value следует писать числовое значение.             
    {
        comparison: "gt",
        field:"weight",
        value:50
    }
    
    В случае с параметром field = birthDate (значение даты), в value надо подавать строковое значение отформатированной даты. Кроме того в случае с датой добавляется еще один параметр - pattern, в котором указывается формат подаваемой даты.
    {
        comparison: "eq",
        field:"birthDate",
        value:"1990-02-05",
        pattern:"yyyy-MM-dd"
    }  
    
     
### Guides
The following guides illustrate how to use some features concretely:

     <p>
         Main request body is json formatted object with two main parameters:
 
          <h5>Main usage</h5>
          Example code of minimal mandatory request:
          <code>
              {
                  "first":0,
                  "length":10
              }
          </code>
 
          There is two additional parameters to sort result records.
          Example code:
          <code>
              {
                  "first":0,
                  "length":10,
                  "sortDirection":"ASC",
                  "sortField":"entityFieldName"
              }
          </code>
 
          To get filtered data there is optional request parameter called "rules".
          Example code with filter rules:
          <code>
              {
                  "first":0,
                  "length":10,
                  "sortDirection":"ASC",
                  "sortField":"entityFieldName",
                  "rules":{
                      "operation":OR,
                      "conditions":[
                           {
                               "comparison":"like",
                               "field":"firstName",
                               "type":"string",
                               "value":"%Search word%"
                           },
                           {
                               "comparison":"like",
                               "field":"lastName",
                               "type":"string",
                               "value":"%Search word%"
                           }
                      ],
                      "rules":[]
                  }
              }
          </code>
 
          Filtering with sub-collection field value is in value.
          <code>
              {
                   "first":0,
                   "length":10,
                   "rules":{
                       "operation":"AND",
                       "conditions":[
                           {
                               "comparison":"in",
                               "value":1,
                               "type":"numeric_long",
                               "field":"users.id"
                           }
                       ]
                   }
               }
          </code>
 
      </p>
