package kz.ruslovislo.jpa.uuid;

/**
 * @author Ruslan Temirbulatov on 10/17/19
 * @project pojo-utils
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UuidAbstractRepository<T extends UuidEntity> extends JpaRepository<T, String>, JpaSpecificationExecutor<T> {
}

