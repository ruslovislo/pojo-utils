package kz.ruslovislo.jpa.uuid;

/**
 * @author Ruslan Temirbulatov on 10/17/19
 * @project pojo-utils
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

/**
 * The basic object of all custom jpa entities.
 * @author Ruslan Temirbulatov
 * @xxx bla bla bla
 *
 */
@SuppressWarnings("serial")
@MappedSuperclass
public class UuidEntity implements Serializable{


    @Id
//    @javax.persistence.Id
//    @Indexed(name="id", type="string")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(columnDefinition = "CHAR(36)")
    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    @Column(updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "en_US")
    private Calendar createdDate;

    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "en_US")
    private Calendar modificatedDate;



    @PreUpdate
    protected void preUpdate() {
        modificatedDate = Calendar.getInstance();
    }


    @PrePersist
    protected void prePersist() {
        createdDate = Calendar.getInstance();
        modificatedDate = Calendar.getInstance();
    }
}
