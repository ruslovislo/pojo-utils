package kz.ruslovislo.jpa;

/**
 * @author Ruslan Temirbulatov on 10/17/19
 * @project pojo-utils
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AbstractRepository <T extends IdEntity> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
}

