package kz.ruslovislo.pojo.table;

import com.fasterxml.jackson.databind.ObjectMapper;


import kz.ruslovislo.jpa.AbstractRepository;
import kz.ruslovislo.jpa.IdEntity;
import kz.ruslovislo.jpa.empty.EmptyAbstractRepository;
import kz.ruslovislo.jpa.empty.EmptyEntity;
import kz.ruslovislo.jpa.uuid.UuidAbstractRepository;
import kz.ruslovislo.jpa.uuid.UuidEntity;
import lombok.Data;
import org.springframework.data.domain.*;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Объект, для отправки запроса данных из БД с учетом пагинации, фильтрации и сортировки
 *     <p>
 *         Main request body is json formatted object with two main parameters:
 *
 *         <h5>Main usage</h5>

 *         Example code of minimal mandatory request:
 *         <code>
 *             {
 *                 "first":0,
 *                 "length":10
 *             }
 *         </code>
 *
 *         There is two additional parameters to sort result records.
 *         Example code:
 *         <code>
 *             {
 *                 "first":0,
 *                 "length":10,
 *                 "sortDirection":"ASC",
 *                 "sortField":"entityFieldName"
 *             }
 *         </code>
 *
 *         To get filtered data there is optional request parameter called "rules".
 *         Example code with filter rules:
 *         <code>
 *             {
 *                 "first":0,
 *                 "length":10,
 *                 "sortDirection":"ASC",
 *                 "sortField":"entityFieldName",
 *                 "rules":{
 *                     "operation":OR,
 *                     "conditions":[
 *                          {
 *                              "comparison":"like",
 *                              "field":"firstName",
 *                              "type":"string",
 *                              "value":"%Search word%"
 *                          },
 *                          {
 *                              "comparison":"like",
 *                              "field":"lastName",
 *                              "type":"string",
 *                              "value":"%Search word%"
 *                          }
 *                     ],
 *                     "rules":[]
 *                 }
 *             }
 *         </code>
 *
 *         Filtering with sub-collection field value is in value.
 *         <code>
 *             {
 *                  "first":0,
 *                  "length":10,
 *                  "rules":{
 *                      "operation":"AND",
 *                      "conditions":[
 *                          {
 *                              "comparison":"in",
 *                              "value":1,
 *                              "type":"numeric_long",
 *                              "field":"users.id"
 *                          }
 *                      ]
 *                  }
 *              }
 *         </code>
 *
 *     </p>
 * @list DTO objects
 * @name TableDataRequest
 */
@Data
public class TableDataRequest {

    final static ObjectMapper mapper = new ObjectMapper();
    /**
     * The number of the first record
     * @Required
     */
    private Integer first;
    /**
     * номер страницы
     */
    private Integer start;
    /**
     * The number of the records size to retrieve from the server.
     * @Required
     */
    private Integer length;
    /**
     * The JPA entity field name to make sort records by the field
     */
    private String sortField;
    /**
     * Sort direction. The value can take from options [ASC,DESC]. ASC - is the default value.
     */
    private Sort.Direction sortDirection = Sort.Direction.ASC;

    private Map<String, Object> filter = new HashMap<>();

    private Rules rules;



    public Integer getFirst() {
        if(first==null && start!=null && length!=null){
            first = start * length;
        }
        return first;
    }

    public static <X> Example getFilterExample(TableDataRequest request, Class cls) throws Exception {

        if (request.getFilter().get("self") == null) {
            X x = (X) cls.newInstance();
            Example<X> example = Example.of(x);
            return example;
        }

        X filter = (X) mapper.convertValue(request.getFilter().get("self"), cls);
        String mode = (String) request.getFilter().get("mode");
        ExampleMatcher matcher = null;
        if (mode != null && mode.equals("any")) {
            matcher = ExampleMatcher.matchingAny().withIgnoreCase()
                    .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        } else {
            matcher = ExampleMatcher.matchingAll().withIgnoreCase()
                    .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        }

        Example<X> example = Example.of(filter, matcher);


        return example;
    }


    public static <X extends IdEntity> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, AbstractRepository<X> repo) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        return pageToRestDatatable(request,page);

    }
    public static <X extends UuidEntity> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, UuidAbstractRepository<X> repo) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        return pageToRestDatatable(request,page);
    }
    public static <X extends EmptyEntity,ID> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, EmptyAbstractRepository<X,ID> repo) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        return pageToRestDatatable(request,page);
    }




    public static <T,X extends IdEntity> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, AbstractRepository<X> repo, IBeanConverter<T,X> converter) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        if(converter!=null){
            List<T> list = new LinkedList<>();
            for(X x:page.getContent()){
                list.add(converter.convert(x));
            }
            Page<T> tPage = new PageImpl<T>(list, page.getPageable(),page.getTotalElements());
            return pageToRestDatatable(request,tPage);
        }else
            return pageToRestDatatable(request,page);

    }
    public static <T,X extends UuidEntity> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, UuidAbstractRepository<X> repo, IBeanConverter<T,X> converter) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        if(converter!=null){
            List<T> list = new LinkedList<>();
            for(X x:page.getContent()){
                list.add(converter.convert(x));
            }
            Page<T> tPage = new PageImpl<T>(list, page.getPageable(),page.getTotalElements());
            return pageToRestDatatable(request,tPage);
        }else
            return pageToRestDatatable(request,page);

    }
    public static <T,X extends EmptyEntity,ID> Map<String,Object> findAllRestDatatable(Class cls, TableDataRequest request, EmptyAbstractRepository<X,ID> repo, IBeanConverter<T,X> converter) throws Exception {
        Page<X> page = findAll(cls,request,repo);
        if(converter!=null){
            List<T> list = new LinkedList<>();
            for(X x:page.getContent()){
                list.add(converter.convert(x));
            }
            Page<T> tPage = new PageImpl<T>(list, page.getPageable(),page.getTotalElements());
            return pageToRestDatatable(request,tPage);
        }else
            return pageToRestDatatable(request,page);

    }


    public static <X extends IdEntity> Page<X> findAll(Class cls, TableDataRequest request, AbstractRepository<X> repo) throws Exception {
        return findAll(null,cls,request,repo);
    }

    public static <X extends IdEntity> Page<X> findAll(Class dto, Class cls, TableDataRequest request, AbstractRepository<X> repo) throws Exception {
        if (request.getRules() != null) {
            //return repo.findAll(Utils.getPageRequest(request));
            return repo.findAll(new Filter(dto, request.getRules()), getPageRequest(dto,request));
        }
        Example<X> example = getFilterExample(request, cls);
        return repo.findAll(example, getPageRequest(dto,request));
    }

    public static <X extends IdEntity, T> Page<T> findAll(Class cls, TableDataRequest request, AbstractRepository<X> repo, IBeanConverter<T, X> converter) throws Exception {
        return findAll(null, cls,request,repo,converter);
    }
    public static <X extends IdEntity, T> Page<T> findAll(Class dto, Class cls, TableDataRequest request, AbstractRepository<X> repo, IBeanConverter<T, X> converter) throws Exception {

        Page<X> xPage = null;

        if (request.getRules() != null) {
            //return repo.findAll(Utils.getPageRequest(request));

            xPage = repo.findAll(new Filter(dto, request.getRules()), getPageRequest(dto,request));

        }else{

            Example<X> example = getFilterExample(request, cls);
            xPage = repo.findAll(example, getPageRequest(dto,request));

        }

        List<T> tList = new ArrayList<>();
        for(X x:xPage.getContent())
            tList.add(converter.convert(x));
        return new PageImpl<T>(tList, xPage.getPageable(), xPage.getTotalElements());

    }

    public static <X extends UuidEntity> Page<X> findAll(Class cls, TableDataRequest request, UuidAbstractRepository<X> repo) throws Exception {
        if (request.getRules() != null) {
            //return repo.findAll(Utils.getPageRequest(request));
            return repo.findAll(new Filter(null,request.getRules()), getPageRequest(cls,request));
        }
        Example<X> example = getFilterExample(request, cls);
        return repo.findAll(example, getPageRequest(cls,request));
    }
    public static <X extends EmptyEntity, ID> Page<X> findAll(Class cls, TableDataRequest request, EmptyAbstractRepository<X,ID> repo) throws Exception {
        if (request.getRules() != null) {
            //return repo.findAll(Utils.getPageRequest(request));
            return repo.findAll(new Filter(null,request.getRules()), getPageRequest(cls,request));
        }
        Example<X> example = getFilterExample(request, cls);
        return repo.findAll(example, getPageRequest(cls,request));
    }






    public static PageRequest getPageRequest(Class dto, TableDataRequest request) {

        if(request.getStart()==null) {
            request.setStart(request.getFirst()/request.getLength());
        }



        return getPageRequest(dto,request.getStart(),request.getLength(),request.getSortDirection(),request.getSortField());
    }


    public static String getEntityFieldName(Class dto, String fieldName){
        String rezField = fieldName;
        if(dto!=null) {
            Field field = null;
            while (field==null) {
                Field[] fields = dto.getDeclaredFields();
                if (fields != null && fields.length > 0) {
                    for (Field f : fields) {
                        if (f.getName().equals(fieldName)) {
                            field = f;
                            break;
                        }
                    }
                }

                if(field == null) {
                    dto = dto.getSuperclass();
                    if(dto == null)
                        break;
                }

            }



            if(field!=null) {
                EntityFieldName entityFieldName = field.getAnnotation(EntityFieldName.class);
                if (entityFieldName != null) {
                    rezField = entityFieldName.value();
                }
            }

        }

        return rezField;
    }

    public static PageRequest getPageRequest(Class dto, Integer start, Integer length, Sort.Direction direction, String sortName)  {

        if(sortName==null || sortName.trim().length()==0)
            return PageRequest.of(start, length);
        else {
            String sortField = getEntityFieldName(dto, sortName);

            {
                if(dto!=null) {
                    Field field = null;
                    while (field==null) {
                        Field[] fields = dto.getDeclaredFields();
                        if (fields != null && fields.length > 0) {
                            for (Field f : fields) {
                                if (f.getName().equals(sortName)) {
                                    field = f;
                                    break;
                                }
                            }
                        }

                        if(field == null) {
                            dto = dto.getSuperclass();
                            if(dto == null)
                                break;
                        }

                    }



//                    Field field = dto.getDeclaredField(sortName);

                    if(field!=null) {
                        EntityFieldName entityFieldName = field.getAnnotation(EntityFieldName.class);
                        if (entityFieldName != null) {
                            sortField = entityFieldName.value();
                        }
                    }

                }
            }
            return PageRequest.of(start, length, Sort.by(direction, sortField));

        }
    }

    public static <T> Map<String,Object> pageToRestDatatable(TableDataRequest request, Page<T> page){
        return pageToRestDatatable(request,page,null);
    }

    public static <T> Map<String,Object> pageToRestDatatable(TableDataRequest request, Page<T> page, IBeanConverter converter){

        Map<String, Object> map = new HashMap<>();
        map.put("recordsTotal", page.getTotalElements());
        map.put("recordsFiltered", page.getTotalElements());
        map.put("data", page.getContent());

        return map;
    }

    public static <T> Map<String,Object> pageToRestDatatable(List<T> data, int recordsTotal){

        Map<String, Object> map = new HashMap<>();
        map.put("recordsTotal", recordsTotal);
        map.put("recordsFiltered", recordsTotal);
        map.put("data", data);

        return map;
    }



    public static interface IBeanConverter<T,X>{

        public T convert(X x);

    }

    /**
     * To validate input request.
     * @param request
     * @param rules The minimal request requirements
     * @return
     * @throws Exception
     */
    public static boolean validate(TableDataRequest request, Rules rules)throws Exception{
        if(request.getRules()==null)
            return false;

        if(request.getRules().getOperation()!=rules.getOperation())
            return false;

        if(request.getRules().getConditions()==null || request.getRules().getConditions().size()==0)
            return false;

        for(Condition condition1: rules.getConditions()) {
            boolean finded = false;
            for (Condition condition2 : request.getRules().getConditions()) {
                if(condition1.getField().equals(condition2.getField())
                    && condition1.getType().equals(condition2.getType())
                    && condition1.getComparison().equals(condition2.getComparison())
                ){
                    finded = true;
                    break;
                }
            }
            if (!finded)
                return false;

        }

        return true;
    }

    public static void main(String[] args) {
        TableDataRequest request = new TableDataRequest();
        request.setStart(2);
        request.setLength(10);

        System.out.println(request.getFirst());
    }

}
